import {render} from '@testing-library/react'
import React from 'react'
import Button, {ButtonSize, ButtonType} from './Button'
import Arrow from '../assets/icons/arrow-right.svg'

test('button', () => {
  const {container} = render(
    <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY} iconRight={<Arrow/>} disabled/>
  )
  const button = container.querySelector('button')
  expect(button).toContainHTML('Test title')
  expect(button).toHaveClass('btn-sm')
  expect(button).toHaveClass('btn-primary')
  expect(button).toHaveAttribute('disabled')
  expect(button).toHaveClass('btn-icon-right')
  expect(button).toContainHTML('<svg data-file-name="SvgArrowRight" />')
})

test('button without text', () => {
  const {container} = render(<Button size={ButtonSize.SMALL} type={ButtonType.PRIMARY} iconRight={<Arrow/>}/>)
  const button = container.querySelector('button')
  expect(button).toHaveClass('btn-only-icon')
  expect(button).not.toHaveClass('btn-icon-left')
  expect(button).not.toHaveClass('btn-icon-right')
})

test('button with only text', () => {
  const {container} = render(<Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY}/>)
  const button = container.querySelector('button')
  expect(button).toContainHTML('Test title')
  expect(button).not.toHaveClass('btn-only-icon')
  expect(button).not.toHaveClass('btn-icon-left')
  expect(button).not.toHaveClass('btn-icon-right')
})

describe('types', () => {
  test.each(Object.values(ButtonType))('renders %s type correctly', (type => {
    const {container} = render(<Button title={'Test title'} size={ButtonSize.SMALL} type={type}/>)
    const button = container.querySelector('button')
    const expectedClass = `btn-${type}`
    expect(button).toHaveClass(expectedClass)
  }))
})

describe('sizes', () => {
  test.each(Object.values(ButtonSize))('renders %s size correctly', (size => {
    const {container} = render(<Button title={'Test title'} size={size} type={ButtonType.PRIMARY}/>)
    const button = container.querySelector('button')
    const expectedClass = `btn-${size}`
    expect(button).toHaveClass(expectedClass)
  }))
})
