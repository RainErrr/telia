import React, {FC, ReactElement} from 'react'

interface ButtonProps {
  title?: string
  size: ButtonSize
  type: ButtonType
  disabled?: boolean
  iconLeft?: ReactElement
  iconRight?: ReactElement
}

export enum ButtonSize {
  SMALL = 'sm',
  LARGE = 'lg'
}

export enum ButtonType {
  PRIMARY = 'primary',
  SECONDARY_GREY = 'secondary-grey',
  SECONDARY_WHITE = 'secondary-white',
  PURCHASE = 'purchase',
  WITHDRAWAL = 'withdrawal',
  WITHDRAWAL_GREY = 'withdrawal-grey',
  WITHDRAWAL_WHITE = 'withdrawal-white',
  CUSTOMER_SERVICE = 'customer-service',
  CUSTOMER_SERVICE_GREY = 'customer-service-grey',
  CUSTOMER_SERVICE_WHITE = 'customer-service-white',
  PRIMARY_TEXT = 'primary-text',
  CUSTOMER_SUPPORT_TEXT = 'customer-support-text'
}

const Button: FC<ButtonProps> = (props): ReactElement => {
  const {title, size, type, disabled, iconLeft, iconRight} = props

  const iconPosition = () => {
    const res = []
    if (!title) res.push('btn-only-icon')
    if (iconLeft && title) res.push('btn-icon-left')
    if (iconRight && title) res.push('btn-icon-right')
    return res.join(' ')
  }

  return (
    <button className={`btn btn-${size} btn-${type} ${iconPosition()}`} disabled={disabled}>
      {iconLeft}
      <span className="btn-text">{title || ''}</span>
      {iconRight}
    </button>
  )
}

export default Button