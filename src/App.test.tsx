import App from './App'
import {render} from '@testing-library/react'
import React from 'react'

it('render', () => {
  const {container} = render(<App/>)
  expect(container).toContainHTML('Test title')
})