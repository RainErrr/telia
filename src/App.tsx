import React, {FC, ReactElement} from 'react'
import Button, {ButtonSize, ButtonType} from './components/Button'
import {ReactComponent as ArrowRight} from './assets/icons/arrow-right.svg'
import {ReactComponent as ArrowRightSmall} from './assets/icons/arrow-right-small.svg'
import {ReactComponent as Download} from './assets/icons/download.svg'
import {ReactComponent as DownloadSmall} from './assets/icons/download-small.svg'
import {ReactComponent as ShoppingCart} from './assets/icons/shopping-cart.svg'
import {ReactComponent as ShoppingCartSmall} from './assets/icons/shopping-cart-small.svg'
import {ReactComponent as Withdrawal} from './assets/icons/withdrawal.svg'
import {ReactComponent as WithdrawalSmall} from './assets/icons/withdrawal-small.svg'
import {ReactComponent as Customer} from './assets/icons/customer.svg'
import {ReactComponent as ArrowLeft} from './assets/icons/arrow-left.svg'
import {ReactComponent as ArrowLeftSmall} from './assets/icons/arrow-left-small.svg'

const App: FC = (): ReactElement => {
  return (
    <div className="container-fluid">
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title Test title Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY}/>
        <Button title={'Test title Test title Test title Test title Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY} iconLeft={<Download/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY} iconLeft={<DownloadSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_GREY}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.SECONDARY_GREY}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_GREY} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.SECONDARY_GREY} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_GREY} iconLeft={<Download/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.SECONDARY_GREY} iconLeft={<DownloadSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_GREY} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_WHITE}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.SECONDARY_WHITE}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_WHITE} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.SECONDARY_WHITE} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_WHITE} iconLeft={<Download/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.SECONDARY_WHITE} iconLeft={<DownloadSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.SECONDARY_WHITE} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PURCHASE}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PURCHASE}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PURCHASE} iconLeft={<ShoppingCart/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PURCHASE} iconLeft={<ShoppingCartSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PURCHASE} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL} iconLeft={<Withdrawal/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL} iconLeft={<WithdrawalSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_GREY}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL_GREY}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_GREY} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL_GREY} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_GREY} iconLeft={<Withdrawal/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL_GREY} iconLeft={<WithdrawalSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_GREY} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_WHITE}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL_WHITE}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_WHITE} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL_WHITE} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_WHITE} iconLeft={<Withdrawal/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.WITHDRAWAL_WHITE} iconLeft={<WithdrawalSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.WITHDRAWAL_WHITE} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE} iconLeft={<Customer/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE} iconLeft={<DownloadSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_GREY}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE_GREY}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_GREY} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE_GREY} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_GREY} iconLeft={<Customer/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE_GREY} iconLeft={<DownloadSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_GREY} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_WHITE}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE_WHITE}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_WHITE} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE_WHITE} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_WHITE} iconLeft={<Customer/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE_WHITE} iconLeft={<DownloadSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE_WHITE} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY_TEXT}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY_TEXT}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY_TEXT} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY_TEXT} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY_TEXT} iconLeft={<ArrowLeft/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.PRIMARY_TEXT} iconLeft={<ArrowLeftSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.PRIMARY_TEXT} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SUPPORT_TEXT}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SUPPORT_TEXT}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SUPPORT_TEXT} iconRight={<ArrowRight/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SUPPORT_TEXT} iconRight={<ArrowRightSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SUPPORT_TEXT} iconLeft={<ArrowLeft/>}/>
        <Button title={'Test title'} size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SUPPORT_TEXT} iconLeft={<ArrowLeftSmall/>}/>
        <Button title={'Test title'} size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SUPPORT_TEXT} disabled/>
      </div>
      <div className="d-flex justify-content-evenly mt-4">
        <Button size={ButtonSize.LARGE} type={ButtonType.PRIMARY} iconRight={<ArrowRight/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.PRIMARY} iconRight={<ArrowRight/>} disabled/>
        <Button size={ButtonSize.SMALL} type={ButtonType.PRIMARY} iconRight={<ArrowRightSmall/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.SECONDARY_GREY} iconRight={<ArrowRight/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.SECONDARY_GREY} iconRight={<ArrowRight/>} disabled/>
        <Button size={ButtonSize.SMALL} type={ButtonType.SECONDARY_GREY} iconRight={<ArrowRightSmall/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.SECONDARY_WHITE} iconRight={<ArrowRight/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.SECONDARY_WHITE} iconRight={<ArrowRight/>} disabled/>
        <Button size={ButtonSize.SMALL} type={ButtonType.SECONDARY_WHITE} iconRight={<ArrowRightSmall/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.PURCHASE} iconRight={<ArrowRight/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.PURCHASE} iconRight={<ArrowRight/>} disabled/>
        <Button size={ButtonSize.SMALL} type={ButtonType.PURCHASE} iconRight={<ArrowRightSmall/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE} iconRight={<ArrowRight/>}/>
        <Button size={ButtonSize.LARGE} type={ButtonType.CUSTOMER_SERVICE} iconRight={<ArrowRight/>} disabled/>
        <Button size={ButtonSize.SMALL} type={ButtonType.CUSTOMER_SERVICE} iconRight={<ArrowRightSmall/>}/>
      </div>
    </div>
  )
}

export default App
