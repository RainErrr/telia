#Assigment

Implement generic button

##Installation

* Make sure you have at least node 15 and relevant npm
* `git clone <project>` to clone project
* `npm i` for node modules

##Development:

###Run UI
`npm run dev`

###Test
`npm run test`

##Stack
* Typescript
* ReactJS
* Vite
* Bootstrap 5